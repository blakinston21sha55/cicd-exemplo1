FROM python:3.6

ARG teste
ARG teste2
ENV teste=${teste} 
ENV teste2=${teste2}
COPY . /usr/src/app
WORKDIR /usr/src/app
cmd ["python","teste.py"]
